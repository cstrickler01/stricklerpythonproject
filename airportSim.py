import sys
class Airport:
    """
    The Airport class contains plane objects and places them in a priority queue
    """

    def __init__(self, plane):
        """
        Initialize the attributes of an object.
        """
        self.name = plane[0]
        self.taxi = plane[1]
        self.requested = plane[2]
        self.duration = plane[3]
        self.actual = int(plane[2])
        self.end = int(self.actual) + int(self.duration) - 1

    def __str__(self):
        """
        Prints out the name, taxi time, actual runway time, and end time of a given plane object
        """
        return "%s, %s, %s, %s" % (self.name, self.taxi, self.actual, self.end)


    def is_empty(self, list):
        """
        Check if a list is empty
        """
        return len(list) == 0

    def enqueue(self, plane, queue):
        """
        Places a plane in the priority queue by comparing its requested time to the requested times of planes already queued
        Inserted is a bool that is initialized to false. It checks if the plane has already been inserted or note
        The first if statement places the plane at the first place in the queue if the queue is is empty
        The for loop iterates across the queue comparing requested times
        The last if statement is used to append the plane to the end of the list
        """
        inserted = False

        if len(queue) == 0:
            queue.append(plane)
            inserted = True

        for i in range(len(queue)):
            if int(queue[i].requested) > int(plane.requested) and inserted == False:
                queue.insert(i, plane)
                inserted = True

        if inserted == False:
            queue.append(plane)

        return queue

    def dequeue(self, time, queue):
        """
        Assigns a plane that has taken off to "item", and deletes that plane from the queue
        If statement handles 0 case, where there is only one element in the queue.
        For loop iterates across the queue, dequeueing any planes that have taken off
        """
        item = 0
        if len(queue) == 1 and queue[0].end < time:
            item = queue[0]
            del queue[0]

        for i in range(len(queue)-1):
            if queue[i].end < time:
                item = queue[i]
                del queue[i]

        return item

    def fix_queue(self, queue):
        """
        Fixes the queue by iterating across and seeing if queue[i] element ends after the queue[i+1] element begins
        If so, the queue[i+1] element's actual begin time is changed to the queue[i] element's end time plus one, and the end time is adjusted
        """
        for i in range(len(queue)-1):
            if int(queue[i].end) > int(queue[i+1].actual):
                queue[i+1].actual = int(queue[i].end) + 1
                queue[i+1].end = int(queue[i+1].actual) + int(queue[i+1].duration)

    def track_finished_flights(self, todays_flights, flight):
        """
        Planes that have been dequeued are added to the todays_flights list
        """
        if flight != 0:
            todays_flights.append(flight)

    def print_finished(self, todays_flights):
        """
        Prints out what today's schedule looked like
        """
        print("")
        print("Today's schedule looked like: ")
        print("-------------------------")
        for i in range(len(todays_flights)):
            print("%s.) %s, (%s - %s)" % (i+1, todays_flights[i].name, todays_flights[i].actual, todays_flights[i].end))

    def printQ(self, time, queue):
        """
        Print the current status of the queue
        """
        print("Time ", time, end=": ")
        for i in range(len(queue)):
            if int(queue[i].actual) <= time:
                print("%s (started at %s, ends at %s) " % (queue[i].name, queue[i].actual, queue[i].end), end="")
            else:
                print("%s (scheduled for %s, ends at %s) " % (queue[i].name, queue[i].actual, queue[i].end), end="")
        print("")

    def read_in_file(self, list, file):
        """
        Read in file line by line to make each line a plane object
        The planes are all added to a list
        """
        with open(file, "r") as ins:
            for line in ins:
                currentline = line.split(",")
                plane = Airport(currentline)
                list.append(plane)

    def get_taxied_planes(self, time, planes_in_file, queue):
        """
        Grab planes with the given taxi time from the list
        The temp list stores the indicies of the planes with that taxi time in descending order
        The list is then passed to a function to delete the planes
        """
        temp = []
        for i in range(len(planes_in_file)):
            if int(planes_in_file[i].taxi) == time:
                self.enqueue(planes_in_file[i], queue)
                temp.insert(0, i)

        self.delete_queued_from_taxied(temp, planes_in_file)

    def delete_queued_from_taxied(self, temp, planes_in_file):
        """
        Deletes the given indicies from the planes_in_file
        """
        for i in range(len(temp)):
            del planes_in_file[temp[i]]

def main():
    #Plane is a temporary plane object, and isn't used at all
    plane = ["Str", 0, 0, 0]
    airport = Airport(plane)
    file = sys.argv[1]
    #Queue is the priority queue, todays_flights are the planes take off, and planes_in_file are the planes left to enqueue
    queue = []
    todays_flights = []
    planes_in_file = []

    airport.read_in_file(planes_in_file, file)
    empty = 1 #Empty will represent the length of the queue
    time = 0

    while len(planes_in_file) or empty > 0:
        taken_off = airport.dequeue(time, queue)
        airport.get_taxied_planes(time, planes_in_file, queue)
        airport.fix_queue(queue)

        #If the airport isn't empty or if there are still planes to be enqueued, print the queue
        if airport.is_empty(queue) == False or len(planes_in_file) > 0:
            airport.printQ(time, queue)

        #Add any dequeued flights to the record of today's flights
        airport.track_finished_flights(todays_flights, taken_off)
        empty = len(queue)
        time = time + 1

    #Print the record of today's flights
    airport.print_finished(todays_flights)


if __name__ == '__main__':
    main()
